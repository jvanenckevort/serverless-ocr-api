#!/bin/bash -x

set -e

rm -rf layer
docker build -t ocr_layer .
CONTAINER=$(docker run -d ocr_layer false)
docker cp $CONTAINER:/opt/build-dist layer
docker rm $CONTAINER
