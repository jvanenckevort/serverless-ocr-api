import base64
import requests
import pytesseract

from PIL import Image
from io import BytesIO


def url(event, context):
    url = event["body"]
    res = requests.get(url)
    img = Image.open(BytesIO(res.content))
    txt = pytesseract.image_to_string(img)
    return {"statusCode": 200, "body": txt}


def file(event, context):
    content = base64.b64decode(event['body'])
    img = Image.open(BytesIO(content))
    txt = pytesseract.image_to_string(img)
    return {"statusCode": 200, "body": txt}
